import { combineReducers } from 'redux';
import { productReducer } from './products.red';

export default combineReducers({
    products: productReducer
})

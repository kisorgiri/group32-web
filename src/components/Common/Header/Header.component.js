
import React from 'react';
import './Header.component.css'
import { Link, withRouter } from 'react-router-dom';


const HeaderComponent = (props) => {
    const logout = () => {
        localStorage.clear();
        // todo navigate to login page
        props.history.push('/');
    }
    const currentUser = JSON.parse(localStorage.getItem('user'));
    
    const content = props.isLoggedIn
        ? <ul className="header-list">
            <li className="header-item">
                <Link to="/home">Home</Link>
            </li>
            <li className="header-item">
                <Link to="/about">About</Link>
            </li>
            <li className="header-item">
                <Link to="/contact">Contact</Link>
            </li>
            <li className="header-item">
                <button
                    className="btn btn-success logout"
                    onClick={logout}
                >
                    logout
                    </button>
            </li>
            <li className="user-info">
                <p>{currentUser.username}</p>
            </li>
        </ul>
        : <ul className="header-list">
            <li className="header-item">
                <Link to="/home">Home</Link>

            </li>
            <li className="header-item">
                <Link to="/register">Register</Link>

            </li>
            <li className="header-item">
                <Link to="/">Login</Link>

            </li>
        </ul>
    return (
        <div className="header">
            {content}
        </div>
    )
}

export const Header = withRouter(HeaderComponent);


import React from 'react'
import './Sidebar.component.css'
import { Link } from 'react-router-dom';


export const SideBar = (props) => {
    return props.isLoggedIn
        ?
        <div className="sidebar">
            <Link className="sidebar_item" to="/dashboard">Dashboard </Link>
            <Link className="sidebar_item" to="/add_product">Add Product </Link>
            <Link className="sidebar_item" to="/view_products">View Product </Link>
            <Link className="sidebar_item" to="/search_product">Search Product </Link>
            <Link className="sidebar_item" to="/chat">Messages </Link>
            
            <Link className="sidebar_item" to="/profile">Profile </Link>
        </div>
        : null

}




import React from 'react';

export const Button = (props) => {
    const disabledLabel = props.disabledLabel || 'submitting...'
    const enabledLabel = props.enabledLabel || 'submit'

    let btn = props.isSubmitting
        ? <button disabled className="btn btn-info m-t-15">{disabledLabel}</button>
        : <button disabled={props.isDisabled} type="submit" className="btn btn-primary m-t-15">{enabledLabel}</button>

    return btn;
}

import React, { Component } from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import { ForgotPassword } from './Auth/ForgotPassword/ForgotPassword.component';
import { Login } from './Auth/Login/Login.component';
import { Register } from './Auth/Register/Register.component';
import { ResetPassword } from './Auth/ResetPassword/ResetPassword.component';
import { Chat } from './Common/Chat/Chat.component';
import { Header } from './Common/Header/Header.component';
import { SideBar } from './Common/Sidebar/Sidebar.component';
import { AddProduct } from './Products/AddProduct/AddProduct.component';
import { EditProduct } from './Products/EditProduct/EditProduct.component';
import { DetailsLanding } from './Products/ProductDetails/DetailsLanding/DetailsLanding.component';
import SearchComponent from './Products/SearchProduct/SearchProduct.component';
import { ViewProducts } from './Products/VIewProducts/ViewProducts.component';


const Home = (props) => {
    console.log('props in home >>', props);
    return <p>Home Page</p>
}
const About = (props) => {
    return <p>About Page</p>
}

const Contact = (props) => {
    return <p>Contact Page</p>
}
const Dashboard = (props) => {
    return (
        <>
            <h3>Welcome to Our Store</h3>
            <p> Please use side navigation menu or contact system administrator for support</p>
        </>
    )
}

const NotFound = (props) => {
    return (
        <div>
            <p>Not Found</p>
            <img src="./images/notfound.png"></img>
        </div>
    )
}

const ProtectedRoute = ({ component: Component, ...rest }) => {
    return <Route {...rest} render={routeProps => (
        localStorage.getItem('token')
            ? <div>
                <Header isLoggedIn={true}></Header>
                <SideBar isLoggedIn={true}></SideBar>
                <div className="main">
                    <Component {...routeProps} ></Component>
                </div>
            </div>
            : <Redirect to="/" ></Redirect>
    )}></Route>
}

const PublicRoute = ({ component: Component, ...rest }) => {
    return <Route {...rest} render={routeProps => (
        <div>
            <Header isLoggedIn={localStorage.getItem('token') ? true : false}></Header>
            <SideBar isLoggedIn={localStorage.getItem('token') ? true : false}></SideBar>

            <div className="main">
                <Component {...routeProps} ></Component>
            </div>
        </div>
    )}></Route>
}
export const AppRouting = (props) => {
    return (
        <BrowserRouter>
            <Switch>
                <PublicRoute exact path="/" component={Login}></PublicRoute>
                <PublicRoute path="/register" component={Register} ></PublicRoute>
                <PublicRoute path="/forgot_password" component={ForgotPassword} ></PublicRoute>
                <PublicRoute path="/reset_password/:token" component={ResetPassword} ></PublicRoute>
                <PublicRoute path="/home" component={Home}></PublicRoute>
                <ProtectedRoute path="/dashboard" component={Dashboard}></ProtectedRoute>
                <ProtectedRoute path="/add_product" component={AddProduct}></ProtectedRoute>
                <ProtectedRoute path="/view_products" component={ViewProducts}></ProtectedRoute>
                <ProtectedRoute path="/edit_product/:id" component={EditProduct}></ProtectedRoute>
                <ProtectedRoute path="/product_details/:id" component={DetailsLanding}></ProtectedRoute>
                <ProtectedRoute path="/search_product" component={SearchComponent}></ProtectedRoute>
                <ProtectedRoute path="/chat" component={Chat}></ProtectedRoute>
                <PublicRoute component={NotFound}></PublicRoute>
            </Switch>
        </BrowserRouter>
    )
}


// revision
// react-router-dom

// BrowserRouter // wrapper
// Route ==> routing configuration
// Route will have attributes like path component exact
// Route will add props to its component
// history  ==> functions
// macth ==>dynamic url value
// location ==> routing data

// Link for clicking navigation
// Swtich to ensure one component is loaded


import React, { Component } from 'react';
import { Button } from './../../Common/Button/Button.component';
import { Link } from 'react-router-dom';
import { httpClient } from '../../../util/httpClient';
import { notify } from '../../../util/toastr';
import { handleError } from '../../../util/errorHandler';

const defaultForm = {

    password: '',
    confirmPassword: '',

}
export class ResetPassword extends Component {

    constructor() {
        super();
        this.state = {
            data: {
                ...defaultForm
            },
            error: {
                ...defaultForm
            },
            isValidForm: false,
            isSubmitting: false
        }
    }

    componentDidMount() {
        this.userId = this.props.match.params['token']
    }

    handleChange = e => {
        const { name, value } = e.target;
        this.setState(pre => ({
            data: {
                ...pre.data,
                [name]: value
            }
        }), () => {
            this.validateForm(name);
        })
    }

    validateForm = (fieldName) => {
        let errMsg;

        switch (fieldName) {
            case 'password':
                errMsg = this.state.data[fieldName]
                    ? this.state.data['confirmPassword']
                        ? this.state.data['confirmPassword'] === this.state.data[fieldName]
                            ? ''
                            : 'password did not match'
                        : this.state.data[fieldName].length > 8
                            ? ''
                            : 'weak password'
                    : 'required field*';

                break;

            case 'confirmPassword':
                errMsg = this.state.data[fieldName]
                    ? this.state.data['password']
                        ? this.state.data['password'] === this.state.data[fieldName]
                            ? ''
                            : 'password didnot match'
                        : this.state.data[fieldName].length > 8
                            ? ''
                            : 'weak password'
                    : 'required field*'
                break;
            default:
                break;
        }

        this.setState(pre => ({
            error: {
                ...pre.error,
                [fieldName]: errMsg
            }
        }), () => {
            const errors = Object
                .values(this.state.error)
                .filter(err => err);

            this.setState({
                isValidForm: errors.length === 0
            })
        })
    }

    handleSubmit = e => {
        e.preventDefault();
        this.setState({
            isSubmitting: true
        })

        httpClient.POST(`/auth/reset-password/${this.userId}`, this.state.data)
            .then((response) => {
                notify.showSuccess('Password Reset Successfull Please Login')
                this.props.history.push('/');
            })
            .catch((err) => {
                handleError(err);
                this.setState({
                    isSubmitting: false
                })
            })
    }

    render() {
        return (
            <div className="auth-box">
                <h2>Reset Password</h2>
                <p>Please choose your password wisely</p>
                <form className="form-group" onSubmit={this.handleSubmit} noValidate>

                    <label>Password</label>
                    <input type="password" className="form-control" name="password" placeholder="Password" onChange={this.handleChange}></input>
                    <p className="danger">{this.state.error.password}</p>
                    <label>Confirm Password</label>
                    <input type="password" className="form-control" name="confirmPassword" placeholder="Confirm Password" onChange={this.handleChange}></input>
                    <p className="danger">{this.state.error.confirmPassword}</p>

                    <Button
                        isSubmitting={this.state.isSubmitting}
                        isDisabled={!this.state.isValidForm}
                    >

                    </Button>
                </form>
                <p>Back to <Link to="/">Login</Link></p>
            </div>
        )
    }
}

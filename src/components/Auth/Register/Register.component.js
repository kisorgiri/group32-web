import React, { Component } from 'react';
import { Button } from './../../Common/Button/Button.component';
import { Link } from 'react-router-dom';
import { httpClient } from '../../../util/httpClient';
import { notify } from '../../../util/toastr';
import { handleError } from '../../../util/errorHandler';
import './Register.component.css'

const defaultForm = {
    name: '',
    email: '',
    username: '',
    password: '',
    confirmPassword: '',
    dob: '',
    gender: '',
    phoneNumber: '',
    permanetAddress: '',
    temporaryAddress: '',
}
export class Register extends Component {

    constructor() {
        super();
        this.state = {
            data: {
                ...defaultForm
            },
            error: {
                ...defaultForm
            },
            isValidForm: false,
            isSubmitting: false
        }
    }

    handleChange = e => {
        const { name, value } = e.target;
        this.setState(pre => ({
            data: {
                ...pre.data,
                [name]: value
            }
        }), () => {
            this.validateForm(name);
        })
    }

    validateForm = (fieldName) => {
        let errMsg;

        switch (fieldName) {
            case 'username':
                errMsg = this.state.data[fieldName]
                    ? this.state.data[fieldName].length > 6
                        ? ''
                        : 'username must be 6 characters long'
                    : 'required field*'

                break;
            case 'password':
                errMsg = this.state.data[fieldName]
                    ? this.state.data['confirmPassword']
                        ? this.state.data['confirmPassword'] === this.state.data[fieldName]
                            ? ''
                            : 'password did not match'
                        : this.state.data[fieldName].length > 8
                            ? ''
                            : 'weak password'
                    : 'required field*';

                break;

            case 'confirmPassword':
                errMsg = this.state.data[fieldName]
                    ? this.state.data['password']
                        ? this.state.data['password'] === this.state.data[fieldName]
                            ? ''
                            : 'password didnot match'
                        : this.state.data[fieldName].length > 8
                            ? ''
                            : 'weak password'
                    : 'required field*'
                break;

            case 'email':
                errMsg = this.state.data[fieldName]
                    ? this.state.data[fieldName].includes('@') && this.state.data[fieldName].includes('.com')
                        ? ''
                        : 'invalid email'
                    : 'required field*'
                break;

            default:
                break;
        }

        this.setState(pre => ({
            error: {
                ...pre.error,
                [fieldName]: errMsg
            }
        }), () => {
            const errors = Object
                .values(this.state.error)
                .filter(err => err);

            this.setState({
                isValidForm: errors.length === 0
            })
        })
    }

    handleSubmit = e => {
        e.preventDefault();
        this.setState({
            isSubmitting: true
        })
        // api call
        // this.state.data
        httpClient.POST('/auth/register', this.state.data)
            .then((response) => {
                notify.showSuccess('Registration Successfull please check your inbox to activate your account')
                this.props.history.push('/');
            })
            .catch((err) => {
                handleError(err);
                this.setState({
                    isSubmitting: false
                })
            })
    }

    render() {
        // console.log('render at second');
        return (
            <div className="register-box">
                <h2>Register</h2>
                <p>Please provide necessary details to register</p>
                <form className="form-group" onSubmit={this.handleSubmit} noValidate>
                    <label>Name</label>
                    <input type="text" className="form-control" name="name" placeholder="Name" onChange={this.handleChange}></input>
                    <label>Email</label>
                    <input type="text" className="form-control" name="email" placeholder="Email" onChange={this.handleChange}></input>
                    <p className="danger">{this.state.error.email}</p>

                    <label>Phone Number</label>
                    <input type="number" className="form-control" name="phoneNumber" placeholder="Phone Number" onChange={this.handleChange}></input>
                    <label>Username</label>
                    <input type="text" className="form-control" name="username" placeholder="Username" onChange={this.handleChange}></input>
                    <p className="danger">{this.state.error.username}</p>
                    <label>Password</label>
                    <input type="password" className="form-control" name="password" placeholder="Password" onChange={this.handleChange}></input>
                    <p className="danger">{this.state.error.password}</p>
                    <label>Confirm Password</label>
                    <input type="password" className="form-control" name="confirmPassword" placeholder="Confirm Password" onChange={this.handleChange}></input>
                    <p className="danger">{this.state.error.confirmPassword}</p>

                    <label>Tempory Address</label>
                    <input type="text" className="form-control" name="temporyAddress" placeholder="Tempory Address" onChange={this.handleChange}></input>
                    <label>Permanent Address</label>
                    <input type="text" className="form-control" name="permanentAddress" placeholder="Permanent Address" onChange={this.handleChange}></input>
                    <label>D.O.B</label>
                    <input type="date" className="form-control" name="dob" onChange={this.handleChange}></input>
                    <label>Gender</label>
                    <input type="text" className="form-control" name="gender" placeholder="Gender" onChange={this.handleChange}></input>

                    <Button
                        isSubmitting={this.state.isSubmitting}
                        isDisabled={!this.state.isValidForm}
                    >

                    </Button>
                </form>
                <p>Back to <Link to="/">Login</Link></p>
            </div>
        )
    }
}

import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button } from './../../Common/Button/Button.component';
import { httpClient } from '../../../util/httpClient';
import { handleError } from './../../../util/errorHandler'
import { notify } from '../../../util/toastr';
import './Login.component.css'

// class based component
const defaultForm = {
    username: '',
    password: ''
}
export class Login extends Component {
    constructor() {
        super();
        this.state = {
            data: {
                ...defaultForm
            },
            error: {
                ...defaultForm
            },
            remember_me: false,
            isValidForm: false,
            isSubmitting: false,
            showPassword: false
        };
    }

    componentDidMount() {
        var remember_me = localStorage.getItem('remember_me');

        if (remember_me === 'true') {
            this.props.history.push('/dashboard/js')
        }
    }

    handleChange = (event) => {
        let { name, value, type, checked } = event.target;
        if (type === 'checkbox') {
            return this.setState({
                remember_me: checked
            })
        }
        this.setState(pre => ({
            data: {
                ...pre.data,
                [name]: value
            }
        }), () => {
            this.validateForm(name);
        })

    }

    showPassword = () => {
        this.setState({
            showPassword: !this.state.showPassword
        })
    }

    validateForm = fieldName => {
        let errMsg = this.state.data[fieldName]
            ? ''
            : 'required field'

        this.setState(pre => ({
            error: {
                ...pre.error,
                [fieldName]: errMsg
            }
        }), () => {
            const errors = Object.values(this.state.error)
                .filter(err => err);
            this.setState({
                isValidForm: errors.length === 0
            })
        })
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.setState({
            isSubmitting: true
        })

        httpClient.POST('/auth/login', this.state.data)
            .then(response => {
                // localstorage
                localStorage.setItem('token', response.data.token);
                localStorage.setItem('user', JSON.stringify(response.data.user));
                localStorage.setItem('remember_me', this.state.remember_me);
                this.props.history.push('/dashboard')
                // navigate to dashboard
                notify.showSuccess(`Welcome ${response.data.user.username}`)
            })
            .catch(err => {
                handleError(err);
                this.setState({
                    isSubmitting: false
                })
            })
    }

    render() {
        // render method is mandatory
        // render method must return single html node
        // ui logic goes here


        return (
            <div className="login-box">
                <h2>Login</h2>
                <p>Please Login to start your session</p>
                <form className="form-group" onSubmit={this.handleSubmit} >
                    <label htmlFor="username">Username</label>
                    <input type="text" className="form-control" name="username" id="username" placeholder="Username" onChange={this.handleChange}></input>
                    <p className="danger">{this.state.error.username}</p>
                    <label htmlFor="password"> Password</label>
                    <input type={this.state.showPassword ? 'text' : 'password'} className="form-control" name="password" id="password" placeholder="Password" onChange={this.handleChange}></input>
                    <p style={{ cursor: 'pointer', color: 'blue' }} onClick={this.showPassword}>
                        {this.state.showPassword ? 'hide' : 'show'} password
                    </p>
                    <p className="danger">{this.state.error.password}</p>
                    <input id="remember_me" type="checkbox" name="remember_me" onChange={this.handleChange}></input>
                    <label style={{ marginLeft: '10px' }} htmlFor="remember_me"> Remember Me </label>
                    <br />
                    <Button
                        isSubmitting={this.state.isSubmitting}
                        isDisabled={!this.state.isValidForm}
                        enabledLabel="Login"
                        disabledLabel="logining in..."
                    >

                    </Button>
                </form>
                <p>Don't have an Account?</p>
                <p style={{ float: 'left' }}>Register <Link to="/register">here</Link></p>
                <p style={{ float: 'right' }}><Link to="/forgot_password">forgot password?</Link></p>
            </div >
        )
    }
}

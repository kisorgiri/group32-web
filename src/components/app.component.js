import React from 'react';
import { AppRouting } from './app.routing';
import { ToastContainer } from 'react-toastify';
import { Provider } from 'react-redux';
import { store } from './../store';

import 'react-toastify/dist/ReactToastify.css';

function AppComponent(props) {
    return (
        <div>
            <Provider store={store}>
                <AppRouting></AppRouting>

            </Provider>
            <ToastContainer></ToastContainer>

        </div>
    )
}
export const App = AppComponent


// component 
// component is basic building block of react
// component is used to create a user interface

// component will always return single html node

// component can be written in two ways
// functional component
// class based component

// component are of two types in context with data
// stateful component (class based component before 16.8)
// stateless component (functional component)

// 16.8 ==> hooks==> functional component can have state

// thorough out course
// functional component for stateless
// class based component for statefull

// state and props (component's data)
// state ==> data withinn a component
// props ==> incoming data

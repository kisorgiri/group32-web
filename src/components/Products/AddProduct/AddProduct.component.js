import React, { Component } from 'react'
import { handleError } from '../../../util/errorHandler'
import { httpClient } from '../../../util/httpClient'
import { notify } from '../../../util/toastr'
import { ProductForm } from '../ProductForm/ProudctForm.component'

export class AddProduct extends Component {
    constructor() {
        super()

        this.state = {
            isSubmitting: false
        }
    }

    add = (data, files) => {
        this.setState({
            isSubmitting: true
        })
        httpClient.UPLOAD('POST', '/product', data, files)
            .then(response => {
                notify.showSuccess("Product Added Successfully");
                this.props.history.push('/view_products');
            })
            .catch(err => {
                this.setState({
                    isSubmitting: false
                })
                handleError(err);
            })
    }

    render() {
        return (
            <>
                <ProductForm
                    title="Add Product"
                    isSubmitting={this.state.isSubmitting}
                    submitCallback={this.add}
                ></ProductForm>
            </>
        )
    }
}

import React, { Component } from 'react'
import { Button } from './../../Common/Button/Button.component'
const defaultFrom = {
    name: '',
    description: '',
    brand: '',
    category: '',
    price: '',
    color: '',
    modelNo: '',
    tags: '',
    offers: '',
    warrentyStatus: '',
    warrentyPeroid: '',
    size: '',
    discountedItem: '',
    discountType: '',
    discountValue: ''
}
const IMG_URL = process.env.REACT_APP_IMG_URL;
export class ProductForm extends Component {
    constructor() {
        super()

        this.state = {
            data: {
                ...defaultFrom
            },
            error: {
                ...defaultFrom
            },
            isValidForm: false,
            filesToUpload: []
        }
    }

    componentDidMount() {
        const productData = this.props.productData;
        if (productData) {
            this.setState({
                data: {
                    ...defaultFrom,
                    ...productData,
                    discountedItem: productData.discount ? productData.discount.discountedItem : '',
                    discountType: productData.discount
                        ? productData.discount.discountType
                            ? productData.discount.discountType
                            : ''
                        : '',
                    discountValue: productData.discount
                        ? productData.discount.discountValue
                            ? productData.discount.discountValue
                            : ''
                        : ''
                }
            })
        }
    }

    handleChange = (e) => {

        let { name, value, type, checked, files } = e.target;

        if (type === 'file') {
            const { filesToUpload } = this.state;
            filesToUpload.push(files[0]);
            return this.setState({
                filesToUpload
            })
        }

        if (type === 'checkbox') {
            value = checked
        }
        this.setState(pre => ({
            data: {
                ...pre.data,
                [name]: value
            }
        }), () => {
            this.validateForm(name);
        })
    }

    validateForm = (fieldName) => {
        let errMsg;

        switch (fieldName) {
            case 'category':
            case 'name':
                errMsg = this.state.data[fieldName]
                    ? ''
                    : 'required field*';
                break;
            default:
                break;
        }

        this.setState(pre => ({
            error: {
                ...pre.error,
                [fieldName]: errMsg
            }
        }), () => {
            const errors = Object.values(this.state.error)
                .filter(err => err);

            this.setState({
                isValidForm: errors.length === 0
            })
        })
    }

    handleSubmit = e => {
        e.preventDefault();
        this.props.submitCallback(this.state.data, this.state.filesToUpload);
    }

    render() {
        const { productData } = this.props;
        let discountContent = this.state.data.discountedItem
            ? <>
                <br />
                <label>Discount Type</label>
                <select className="form-control" name="discountType" value={this.state.data.discountType} onChange={this.handleChange}>
                    <option disabled value="">(Select Type)</option>
                    <option value="percentage" >Percentage</option>
                    <option value="quantity">Quantity</option>
                    <option value="value">Value</option>
                </select>
                <label>Discount Value</label>
                <input type="text" className="form-control" value={this.state.data.discountValue} name="discountValue" placeholder="Discount Value" onChange={this.handleChange}></input>
            </>
            : null;

        const imageData = productData && productData.images && productData.images.length
            ? <>
                <label>Previous Images</label>
                <br />
                {
                    productData.images.map((image, index) => (
                        <React.Fragment key={index}>
                            <img style={{ display: 'block' }} src={`${IMG_URL}/${image}`} alt="product_image.png" width="180px"></img>
                            <hr />
                        </React.Fragment>
                    ))
                }
                <br />
            </>
            : null;


        return (
            <>
                <h2>{this.props.title}</h2>
                <form onSubmit={this.handleSubmit} className="form-group">
                    <label>Name</label>
                    <input type="text" className="form-control" value={this.state.data.name} name="name" placeholder="Name" onChange={this.handleChange}></input>
                    <label>Description</label>
                    <textarea rows={5} name="description" value={this.state.data.description} placeholder="Description goes here" onChange={this.handleChange} className="form-control"></textarea>
                    <label>Category</label>
                    <input type="text" className="form-control" value={this.state.data.category} name="category" placeholder="Category" onChange={this.handleChange}></input>
                    <label>Brand</label>
                    <input type="text" className="form-control" value={this.state.data.brand} name="brand" placeholder="Brand" onChange={this.handleChange}></input>
                    <label>Price</label>
                    <input type="number" className="form-control" value={this.state.data.price} name="price" onChange={this.handleChange}></input>
                    <label>Color</label>
                    <input type="text" className="form-control" value={this.state.data.color} name="color" placeholder="Color" onChange={this.handleChange}></input>
                    <label>Size</label>
                    <input type="text" className="form-control" value={this.state.data.size} name="size" placeholder="Size" onChange={this.handleChange}></input>
                    <label>Model No.</label>
                    <input type="text" className="form-control" value={this.state.data.modelNo} name="modelNo" placeholder="Model Number" onChange={this.handleChange}></input>
                    <label>Tags</label>
                    <input type="text" className="form-control" value={this.state.data.tags} name="tags" placeholder="Tags" onChange={this.handleChange}></input>
                    <label>Offers</label>
                    <input type="text" className="form-control" value={this.state.data.offers} name="offers" placeholder="Offers" onChange={this.handleChange}></input>
                    <input type="checkbox" name="warrentyStatus" checked={this.state.data.warrentyStatus} onChange={this.handleChange}></input>
                    <label style={{ marginLeft: '10px' }}>Warrenty Status</label>
                    <br />
                    {this.state.data.warrentyStatus
                        ? <>
                            <label>WarrentyPeroid</label>
                            <input type="text" className="form-control" value={this.state.data.warrentyPeroid} name="warrentyPeroid" placeholder="Warrenty Peroid" onChange={this.handleChange}></input>

                        </>
                        : null
                    }
                    <input type="checkbox" name="discountedItem" checked={this.state.data.discountedItem} onChange={this.handleChange}></input>
                    <label>Discounted Item</label>
                    {discountContent}
                    <br />
                    {imageData}
                    <label>Select Image</label>
                    <input className="form-control" type="file" onChange={this.handleChange}></input>
                    <Button
                        isDisabled={!this.state.isValidForm}
                        isSubmitting={this.props.isSubmitting}
                    >

                    </Button>
                </form>
            </>
        )
    }
}

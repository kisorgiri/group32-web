import React, { Component } from 'react'
import { Button } from './../../../Common/Button/Button.component'

export class AddReview extends Component {
    constructor(props) {
        super(props)

        this.state = {
            data: {
                reviewPoint: '',
                reviewMessage: ''
            },
            isSubmitting: false
        }
    }

    handleChange = e => {
        const { name, value } = e.target;

        this.setState((pre) => ({
            data: {
                ...pre.data,
                [name]: value
            }
        }))
    }

    handleSubmit = e => {
        e.preventDefault();
        this.props.addReview(this.state.data)
    }

    render() {
        return (
            <>
                <h2>Add Review</h2>
                <form className="form-group" onSubmit={this.handleSubmit}>
                    <label>Point</label>
                    <input className="form-control" type="number" min="1" max="5" name="reviewPoint" onChange={this.handleChange}></input>
                    <label>Message</label>
                    <input className="form-control" type="text" placeholder="review message here" name="reviewMessage" onChange={this.handleChange}></input>
                    <Button
                        isSubmitting={this.state.isSubmitting}
                        isDisabled={!(this.state.data.reviewMessage && this.state.data.reviewPoint)}
                    >

                    </Button>
                </form>

            </>
        )
    }
}

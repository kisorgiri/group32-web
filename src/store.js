import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers';

const middleware = [thunk]

const initialState = {

    products: {
        products: [],
        isLoading: false,
        pageSize: 5,
        pageNumber: 1
    }
}

export const store = createStore(rootReducer, initialState, applyMiddleware(...middleware));

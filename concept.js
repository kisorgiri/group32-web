// life cycle stage
// init, update, destroy

// init
//  ==> data preparation
// componentDiDmount(){}

// update 
// ==> check differences
// componentDidUpdate(){}
// destroy
// subscription and async task must be closed
// componentWillUnMount(){}


// .env setup for configuration

// error service

// notification service for using toastr

// when using thirdparty utitlity library always try to limit the scope within a file

// redux
// web architecture

// MVC 
// model,view, controller
// bidirectional data flow between model and controller


// FLUX (facebook)
// views===> actions ====> dispatchers====> store
// views ==> react component (UI) action trigger (click)
// actions==> actions is used to dispatch event to dispatchers
// actions can be called from response of API
// dispatchers ==> it sends actions to store to update data
// store contains the application state
// store holds logic to update application state// 
// unidirectional data flow 
// there can be multiple store

// REDUX (dan Abramov)
// views ===>, actions,===> reducers,==> store
// unidirectional dataflow
// the will be single store
// views ==> ui (react component) generate event
// actions ==> determines what needs to be done
// reducer ===> plain functinos --> store changing logic resides on reducer
//store ==> application state

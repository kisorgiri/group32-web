
// ECMAscript 6 and higher versions 

// 1. Object Shorthand
// 2.Object destruct
// Spread Operator
// Rest Operator
// 3. default argument
// 4. arrow notation function
// 5. import and export
// 6. template literals
// 7. class
// 8. block scope
// 9.


//Object shorthand

var name = 'kishor giri';
var address = 'bhaktapur';

var obj = {
    age: '444',
    phone: 444,
    name,
    address
}
console.log(' obj is >>', obj)


function askSomething() {

    return {
        fish: 'fish',
        fruits: 'apple',
        vegitables: 'potato',
        flowers: 'rose'
    }
}
// object destruction

// var { flowers, fruits,xyz } = askSomething();
// console.log('res >>', flowers)
// console.log('res >>', xyz)

const { fish: Kishor } = askSomething();
// console.log('fish >>', fish)
console.log('kishor >>', Kishor)

// spread and rest
// (...)

// spread ==> one by one separate (array and object)
// concatinate 
// mutable lai immutable banaune

var stu1 = {
    name: 'uttam',
    roll: 3,
    house: 4
}
var a = {
    location: 'ktm'
}

// var stu2 = stu1;
var stu2 = {
    ...stu1,
    ...a
}

stu2.name = 'krishna';
console.log('stu1 is >', stu1);
console.log('stu2 is >>', stu2)

// REST operator

var { name: Aa, ...rest } = stu1;
console.log('name >>', Aa)
console.log('rest >>', rest)

// default argument

function sendEmail(details = 'something') {

    console.log('details >>', details)
    // var message = details.message;

}

sendEmail('shi');


// arrow notation function

// function welcome(name) {
//     return name;
// }

// // var welcome = () => {

// // }
// // var welcome = name => {
// //     // if single argument we dont have to write parenthesis
// // }

// var welcome = name => {
//     return name;
// }

// // one liner functional block
// var welcome = name => name;

// core advantages
// ==> it will inherit parent scope(this)

var laptops = [
    {
        name: 'dell',
        color: 'black',
        generation: 'i7'
    },
    {
        name: 'hp',
        color: 'white',
        generation: 'i5'
    }
]

// var i7Laptops = laptops.filter(function (item) {
//     if (item.generation === 'i7') {
//         return true;
//     }
// })
var i7Laptops = laptops.filter(item => item.generation === 'i7');
console.log('i7 laptops >', i7Laptops)


// import and export

// export 
// two ways of export
// named export
// syntax export keyword
// eg const a = 'h';
// export a;
// export const b = 'something'
// export class xx{

// }
// there can be multiple named export in a file

// default export
// syntax export default keyword

// var a = [];
// export default a;
// export default [];

// there can be only single default export within afile

// # there can be both named and default export in a file


// Import
// import totatlly depends on how it is exported
// if named export
// import { exported_name, b, c } from 'location of file';

// if default export
// import some_name from 'location of the file'

// if both named and default export
// import {named_one},abcXyz from 'location of the file'

// template literals
// function welcome(name, address) {
//     // var text = 'welcome ' + name + ' to ' + address
//     var text = `hi ${name} welcome to ${address}`
//     console.log('text', text);
// }
// welcome('rohit', 'nepal')


// block scope
// var will hold functional scope
// let will hold block scope

function h() {
    var xyz = 'hi';
    if (true) {
        var xyz = 'hello'
    }
}
function h() {
    let xyz = 'hi';
    if (true) {
        let xyz = 'hello'
        // xyz has different scope inside if block
    } else {

    }
}



// class 
// class is group of properties, methods and constructor

// eg
class Login {
    status = 'online'
    constructor() {

    }

    getStatus() {
        return this.status;
    }

    setStatus(newStatus) {
        this.status = newStatus;

    }

}

var a = new Login();

// inheritance

class Register extends Login {
    constructor() {
        super();
        // super call is parent class constructor call
        // this.
    }


}
